package DDF;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class WordpressLogin {
	WebDriver driver;
	
	@Test(dataProvider="Wordpressdata")
	public void loginTowordPress(String username, String password) throws InterruptedException
	{
		driver=new FirefoxDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.get("https://www.facebook.com/");
	
	    driver.findElement(By.id("email")).sendKeys(username);
	
		driver.findElement(By.id("pass")).sendKeys(password);
	
		driver.findElement(By.className("uiButton")).click();
		
		Thread.sleep(3000);
		
		System.out.println(driver.getTitle());
		
		Assert.assertTrue(driver.getTitle().contains("Facebook"),"User is not able to login----inavalid credentials");
		
		System.out.println("Page title verified successfully"); 
	
	
	 
	}
	@AfterMethod
	
	public void tearDown()
	{
		driver.quit();
	}
	
	@DataProvider(name="Wordpressdata")
    
	public Object[][] passData()
	{
	 
	Object[][] data=new Object[3][2];
	
	 data[0][0]="admin";
	 
	 data[0][1]="admin@12";
	 
	 data[1][0]="akshatashastri8@gmail.com";
	 
	 data[1][1]="ambruta123";
	 
	 data[2][0]="admin1";
	 
	 data[2][1]="admin@123";
	 
	 return data;
	
		 }
}
