package DDF;
import java.util.concurrent.TimeUnit;

import library.ExceldataConfig;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class WordpressLoginExcel {
	WebDriver driver;
	
	@Test(dataProvider="Wordpressdata")
	public void loginTowordPress(String username, String password) throws InterruptedException
	{
		driver=new FirefoxDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.get("https://www.facebook.com/");
	
	    driver.findElement(By.id("email")).sendKeys(username);
	
		driver.findElement(By.id("pass")).sendKeys(password);
	
		driver.findElement(By.className("uiButton")).click();
		
		Thread.sleep(3000);
		
		System.out.println(driver.getTitle());
		
		Assert.assertTrue(driver.getTitle().contains("Facebook"),"User is not able to login----inavalid credentials");
		
		System.out.println("Page title verified successfully"); 
	
	
	 
	}
	@AfterMethod
	
	public void tearDown()
	{
		driver.quit();
	}
	
	@DataProvider(name="Wordpressdata")
    
	public Object[][] passData()
	{
	ExceldataConfig config=new ExceldataConfig("C:\\Users\\user\\workspace\\Automation\\Testdata\\Inputdata.xlsx"); 
	
	int rows=config.getRowCount(0);
	
	Object[][] data=new Object[3][2];
	
	for(int i=0;i<rows;i++)
	 {
		data[i][0]=config.getdata(0, i, 0);
		data[i][1]=config.getdata(0, i, 1);
		
	 }
	
	 
	 return data;
	
		 }
}

 